import { NativeModules } from 'react-native';

const { RNXprintSdk } = NativeModules;

export default RNXprintSdk;
